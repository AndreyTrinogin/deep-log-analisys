## Version 0.6

analysis_2.py

collect data in table for creating graphics

### Positional arguments:

path_to_deep_ini path to deep ini file

### Optional arguments:

-h, --help show this help message and exit

-l LOG_LEVEL, --log_level LOG_LEVEL  
log level, default 20

-o OUT_DIR, --out_dir OUT_DIR  
output dir to store table, default - start dir

-n TABLE_NAME, --table_name TABLE_NAME  
name of file with table

-r LOG_RESULT, --log_result LOG_RESULT  
name of file with result logs

-d LOG_DEEP, --log_deep LOG_DEEP  
name of file with deep logs

-i LOG_ITERATION, --log_iteration LOG_ITERATION  
name of file with iteration logs

--dir_r DIR_R name of dir with log_result

--dir_d DIR_D name of file with log_deep

--dir_i DIR_I name of file with log_iteration

### Example with all args

```shell
python src/analysis_2.py 
D:\Repos\deep-log-analisys\test_example\new\4\deep_crops-vals-07-02-2021-v4.ini
-l 5
-o D:\Repos\deep-log-analisys\test_example\new
-n kek.txt
-r lg_rt.txt
-d lg_dp
-i lg_it
--dir_r D:\Repos\deep-log-analisys\test_example\new\1
--dir_d D:\Repos\deep-log-analisys\test_example\new\2
--dir_i D:\Repos\deep-log-analisys\test_example\new\3
```
