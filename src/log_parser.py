import re
from collections import defaultdict
from typing import IO


def parse_log_result(l_r_file_name: str, start_iter_num: int, elements_num: int):
    """
    read data for first start_iter_num iterations (not lines!) as data for spec iterations
    and data for other iterations, and save it in 2 dicts, calculate number of iterations

    :param l_r_file_name: file name to read
    :param start_iter_num: number of first real iteration
    :param elements_num: number of elements in one iteration
    :return: 2 dicts and int: res_start_data, res_data, iterations count
    """
    start_take_param = 3  # because we need element number from log result
    step = 3
    res_data = defaultdict(dict)
    res_start_data = defaultdict(dict)

    count_itr = 0
    with open(l_r_file_name, 'r') as l_r_file:

        while count_itr < start_iter_num + 1:
            iteration = l_r_file.readline().rstrip()
            for i in range(elements_num):
                line_list = l_r_file.readline().split()
                res_start_data[iteration][line_list[3]] = [num for num in
                                                           line_list[start_take_param + step:len(line_list):step]]
            count_itr += 1

        count_itr = 0
        while True:
            iteration = l_r_file.readline().rstrip()
            if len(iteration) == 0:
                break
            for i in range(elements_num):
                line_list = l_r_file.readline().split()
                res_data[iteration][line_list[3]] = [num for num in
                                                     line_list[start_take_param + step:len(line_list):step]]
            count_itr += 1

    return res_start_data, res_data, count_itr


def _get_substr_regexp(regex, line: str):
    t = re.search(regex, line)
    return line[t.start(): t.end()]


def _parse_log_deep_str(regex_it: str, regex_el_num: str, regex_seed: str,
                        line: str, to_store: dict, elem_count: int):
    if line.startswith('file'):
        iteration = _get_substr_regexp(regex_it, line)
        element_num = _get_substr_regexp(regex_el_num, line).split()[3]
        seed = _get_substr_regexp(regex_seed, line).split()[2]

        if to_store[iteration].get(element_num) is None:
            to_store[iteration][element_num] = {}
            to_store[iteration][element_num]['seed'] = seed
            to_store[iteration][element_num]['deep_out'] = line.rstrip()
            elem_count += 1
        else:
            pass

    return elem_count


def parse_log_deep(l_d_file_name: str, iter_count: int, start_iter_num: int, elements_num: int):
    """
    read data for first start_iter_num iterations (not lines!) as data for spec iterations
    and data for other iterations, and save it in 2 dicts

    :param l_d_file_name: file name to read
    :param iter_count: count of 'main' iterations in file
    :param start_iter_num: number of first 'main' iteration
    :param elements_num: number of elements in one iteration
    :return: two dicts, deep_start_data, deep_data
    """
    deep_start_data = defaultdict(dict)
    deep_data = defaultdict(dict)
    regex_it = 'iteration = -?\\d+'
    regex_el_num = ' element number = \\d+'
    regex_seed = 'seed = \\d+'
    with open(l_d_file_name, 'r') as l_d_file:
        elem_count = 0
        while elem_count < (start_iter_num + 1) * elements_num:
            elem_count = _parse_log_deep_str(regex_it=regex_it, regex_el_num=regex_el_num, regex_seed=regex_seed,
                                             line=l_d_file.readline(), to_store=deep_start_data, elem_count=elem_count)

        elem_count = 0
        while elem_count < iter_count * elements_num:
            elem_count = _parse_log_deep_str(regex_it=regex_it, regex_el_num=regex_el_num, regex_seed=regex_seed,
                                             line=l_d_file.readline(), to_store=deep_data, elem_count=elem_count)

    return deep_start_data, deep_data


def _parse_log_iteration_str(l_i_file: IO, line: str,
                             count_param: int,
                             to_store: dict, itr_completed: int):
    iteration = line.rstrip()
    if iteration not in to_store:
        itr_completed += 1

    element_number = l_i_file.readline().split()[3]
    check_param = []
    for i in range(count_param):
        check_param.append(l_i_file.readline().rstrip())

    # skip this values
    l_i_file.readline()  # delta
    l_i_file.readline()  # error

    line = l_i_file.readline().rstrip()
    if line.startswith('original alpha'):
        o_alpha = line.split()[3]
        alpha = l_i_file.readline().split()[2].rstrip()
        accept = l_i_file.readline().rstrip()
    else:
        o_alpha = "init"
        alpha = "init"
        accept = "accept"

    if to_store[iteration].get(element_number) is None:
        to_store[iteration][element_number] = {}
        to_store[iteration][element_number]['check_param'] = check_param
        to_store[iteration][element_number]['o_alpha'] = o_alpha
        to_store[iteration][element_number]['alpha'] = alpha
        to_store[iteration][element_number]['accept'] = accept

    return line, itr_completed, iteration


def parse_log_iteration(l_i_file_name: str, parms: dict):
    count_iter = parms['count_iter']
    start_iter = parms['start_iter']
    count_elem = parms['count_theta']

    count_param = parms['count_param']

    iter_start_data = defaultdict(dict)
    iter_data = defaultdict(dict)
    with open(l_i_file_name, 'r') as l_i_file:
        itr_completed = 0
        line = ''
        iteration = ''
        while itr_completed < start_iter + 1 or len(iter_start_data[iteration]) < count_elem:
            if not line.startswith('iteration'):
                line = l_i_file.readline()
            if line.startswith('iteration'):
                line, itr_completed, iteration = _parse_log_iteration_str(l_i_file=l_i_file, line=line,
                                                                          count_param=count_param,
                                                                          to_store=iter_start_data,
                                                                          itr_completed=itr_completed)

        itr_completed = 0
        line = ''
        iteration = ''
        while itr_completed < count_iter or len(iter_data[iteration]) < count_elem:
            if not line.startswith('iteration'):
                line = l_i_file.readline()
            if line.startswith('iteration'):
                line, itr_completed, iteration = _parse_log_iteration_str(l_i_file=l_i_file, line=line,
                                                                          count_param=count_param,
                                                                          to_store=iter_data,
                                                                          itr_completed=itr_completed)

    return iter_start_data, iter_data
