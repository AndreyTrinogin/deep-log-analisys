import argparse
import logging as lg
import math
import os.path

import numpy as np

from deep_reader import DeepReader
from log_parser import parse_log_result, parse_log_deep, parse_log_iteration
from utils import get_default_logger, get_time


def get_abcdeini_params(deep_ini_file_name: str):
    """
    fill dict with parameters
    Constant params:
        size_trailing - 1200
        size_valid - 300
    Read:
        start_iter
        count_iter
        count_theta
        lbound
        hbound
    Calculated params:
        num_log_pram
        count_param
    :param deep_ini_file_name:
    :return: dict param_name:value
    """

    # TODO Check constants 1200 and 300
    parms = {'count_iter': None,
             'size_trailing': 1200,
             'size_valid': 300}

    with open(deep_ini_file_name, 'r') as deep_ini_file:
        # TODO maybe we need to convert to buffered block reading (anywhere)
        for line in deep_ini_file:
            if line.startswith('[default_settings]'):
                break
            elif line.startswith('start_iter'):
                parms['start_iter'] = int(line.split('=')[1])
            elif line.startswith('count_iter'):
                # temp value for count_iter, means count of elements in one iteration
                parms['count_iter'] = int(line.split('=')[1])
                parms['count_theta'] = int(line.split('=')[1])
            elif line.startswith('lbound'):
                parms['lbound'] = np.array(line.split('=')[1].split('\n')[0].split(';'), dtype=np.float64)
            elif line.startswith('hbound'):
                parms['hbound'] = np.array(line.split('=')[1].split('\n')[0].split(';'), dtype=np.float64)

    parms['count_param'] = len(parms['hbound'])
    parms['num_log_param'] = len(parms['hbound']) + 3

    return parms


def set_param(data, parms):
    for i in range(0, len(parms["lbound"])):
        a = (parms["hbound"][i] + parms["lbound"][i]) / 2.0
        b = (parms["hbound"][i] - parms["lbound"][i]) / 2.0
        data[i] = a + b * math.tanh(float(data[i]))
    return data


def plot_table_rows(r_data: dict, d_data: dict, i_data: dict,
                    parms: dict, dr: DeepReader,
                    out_fldr, out_file_name: str,
                    accept_deep_out: list):
    for iteration in r_data:
        curr_res = r_data[iteration]
        for elem in curr_res:
            curr_elem = elem

            curr_seed = d_data[iteration][curr_elem]['seed']
            curr_deep_out = d_data[iteration][curr_elem]['deep_out']

            curr_check_param = i_data[iteration][curr_elem]['check_param']
            curr_o_alpha = i_data[iteration][curr_elem]['o_alpha']
            curr_alpha = i_data[iteration][curr_elem]['alpha']
            curr_accept = i_data[iteration][curr_elem]['accept']

            dr.create_table(folder=out_fldr, out_file_name=out_file_name,
                            x=r_data[iteration][curr_elem],
                            x_b=set_param(r_data[iteration][curr_elem][0:-3], parms),
                            x_deep=curr_check_param,
                            x_deep_b=set_param(curr_check_param, parms),
                            iteration=iteration.split()[2], element_number=curr_elem, curr_seed=curr_seed,
                            d=curr_deep_out, o_alpha=curr_o_alpha, alpha=curr_alpha, accept=curr_accept)

            if curr_accept == 'accept' or curr_accept == 'accept alpha':
                accept_deep_out.append(curr_deep_out)

    return accept_deep_out


def plot_table(out_fldr, out_file_name,
               parms,
               r_s_data, r_data,
               d_s_data, d_data,
               i_s_data, i_data):
    deep_reader = DeepReader(params=parms)
    deep_reader.create_header(folder=out_fldr, out_file_name=out_file_name,
                              deep_header=d_data['iteration = 0']['0']['deep_out'])

    acc_deep_output = []

    acc_deep_output = plot_table_rows(r_data=r_s_data, d_data=d_s_data, i_data=i_s_data,
                                      parms=parms, dr=deep_reader, out_fldr=out_fldr, out_file_name=out_file_name,
                                      accept_deep_out=acc_deep_output)

    acc_deep_output = plot_table_rows(r_data=r_data, d_data=d_data, i_data=i_data,
                                      parms=parms, dr=deep_reader, out_fldr=out_fldr, out_file_name=out_file_name,
                                      accept_deep_out=acc_deep_output)

    return acc_deep_output


def arg_validate(arg):
    def check_dr(dir_name, arg_name):
        if dir_name is not None and not os.path.isdir(dir_name):
            raise ValueError('Incorrect dir {} in parameter {}'.format(dir_name, arg_name))

    def check_file(dr, fl_name, arg_name):
        if fl_name is not None and not os.path.isfile(os.path.join(dr, fl_name)):
            raise ValueError('Incorrect file {} in parameter {}'.format(fl_name, arg_name))

    if args.path_to_deep_ini is not None and not os.path.isfile(args.path_to_deep_ini):
        raise ValueError('Incorrect file {} in parameter {}'.format(args.path_to_deep_ini, 'path_to_deep_ini'))

    check_file(arg.dir_r, arg.log_result, 'log_result')
    check_file(arg.dir_d, arg.log_deep, 'log_deep')
    check_file(arg.dir_i, arg.log_iteration, 'log_iteration')

    if not 0 < arg.log_level < 100:
        raise ValueError('Incorrect log level{} , available values 0 - 100'.format(arg.log_level))

    check_dr(arg.dir_r, 'dir_r')
    check_dr(arg.dir_d, 'dir_d')
    check_dr(arg.dir_i, 'dir_i')


def choose_dir(fldr, dr):
    return fldr if dr is None else dr


if __name__ == "__main__":
    prsr = argparse.ArgumentParser(description='Script to generate table')
    prsr.add_argument('path_to_deep_ini', type=str, help='path to deep ini file')
    prsr.add_argument('-l', '--log_level', type=int, default=20, help='log level, default 20')
    prsr.add_argument('-o', '--out_dir', type=str, help='output dir to store table, default - start dir')
    prsr.add_argument('-n', '--table_name', type=str, default='table.txt',
                      help='name of file with table')

    prsr.add_argument('-r', '--log_result', type=str, default='log_result.txt',
                      help='name of file with result logs')
    prsr.add_argument('-d', '--log_deep', type=str, default='log_deep',
                      help='name of file with deep logs')
    prsr.add_argument('-i', '--log_iteration', type=str, default='log_iteration.txt',
                      help='name of file with iteration logs')

    prsr.add_argument('--dir_r', type=str, help='name of dir with log_result')
    prsr.add_argument('--dir_d', type=str, help='name of file with log_deep')
    prsr.add_argument('--dir_i', type=str, help='name of file with log_iteration')

    args = prsr.parse_args()
    arg_validate(args)

    get_default_logger(args.log_level)
    lg.info('start: ' + get_time())

    lg.debug(args.path_to_deep_ini)
    lg.debug(args.log_level)

    folder = os.path.dirname(args.path_to_deep_ini)
    dir_r = choose_dir(folder, args.dir_r)
    dir_d = choose_dir(folder, args.dir_d)
    dir_i = choose_dir(folder, args.dir_i)

    log_result_full_name = os.path.abspath(os.path.join(dir_r, args.log_result))
    log_deeps_full_name = os.path.abspath(os.path.join(dir_d, args.log_deep))
    log_iter_full_name = os.path.abspath(os.path.join(dir_i, args.log_iteration))

    params = get_abcdeini_params(args.path_to_deep_ini)
    lg.debug(params)

    result_start_data, result_data, params['count_iter'] = parse_log_result(log_result_full_name,
                                                                            params['start_iter'],
                                                                            params['count_iter'])
    lg.debug('parse_log_result ended: ' + get_time())

    # because params['count_iter'] was read from logs
    lg.debug(params)

    deep_start_data, deep_data = parse_log_deep(log_deeps_full_name,
                                                params['count_iter'],
                                                params['start_iter'],
                                                params['count_theta'])

    lg.debug('parse_log_deep ended: ' + get_time())

    iter_start_data, iter_data = parse_log_iteration(log_iter_full_name, params)

    lg.debug('parse_log_iteration ended: ' + get_time())

    out_dir = folder if args.out_dir is None else args.out_dir

    accept_deep_output = plot_table(out_dir, args.table_name,
                                    params,
                                    result_start_data, result_data,
                                    deep_start_data, deep_data,
                                    iter_start_data, iter_data)

    lg.info('end: ' + get_time())
