import csv
import os.path
from dataclasses import dataclass
from typing import Dict


@dataclass
class DeepReader:
    params: Dict

    def parse_deep_header(self, _str):
        l_list = _str.split(" ")
        header = []
        target = []
        for elem in l_list:
            h = elem.split(":")
            if len(h) == 2:
                header.append(h[0])
        for h in header:
            if "target" in h:
                target.append(h)

        return target

    def parse_deep(self, _str):
        l_list = _str.split(" ")
        res = []
        for elem in l_list:
            h = elem.split(":")
            if len(h) == 2 and "target" in h[0]:
                res.append(h[1])
        return res

    def create_header(self, folder, out_file_name, deep_header):
        filename = folder + os.sep + out_file_name
        deep_output = self.parse_deep_header(deep_header)
        with open(filename, "w") as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            tab = ["element_number"]
            for i in range(self.params["count_param"]):
                tab.append("accept_param[" + str(i) + "]")
            tab.append("w")
            tab.append("error")
            tab.append("delta")
            for i in range(self.params["count_param"]):
                tab.append("accept_param_bounds[" + str(i) + "]")
            for i in range(self.params["count_param"]):
                tab.append("curr_param[" + str(i) + "]")
            for i in range(self.params["count_param"]):
                tab.append("curr_param_bounds[" + str(i) + "]")
            tab.append("iter")
            tab.append("element_number")
            tab.append("seed")
            #  tab.append("deepoutput")
            tab.append("original_alpha")
            tab.append("alpha")
            tab.append("accept")
            for target in deep_output:
                # print(target)
                tab.append(target)
            # tab = ["element_number", "n", "l", "l1", "l2", "l3", "l4", "w", "error", "delta", "n_b", "l_b", "l1_b",
            # "l2_b", "l3_b", "l4_b",  "n_deep", "l_deep", "l1_deep", "l2_deep", "l3_deep", "l4_deep", "n_deep_b",
            # "l_deep_b", "l1_deep_b", "l2_deep_b", "l3_deep_b", "l4_deep_b",  "iter", "element_number", "seed",
            # "deepoutput", "original_alpha", "alpha", "accept"]
            writer.writerow(tab)

    def create_table(self, folder, out_file_name, x, x_b, x_deep, x_deep_b, iteration, element_number, curr_seed, d,
                     o_alpha, alpha,
                     accept):
        filename = folder + os.sep + out_file_name
        with open(filename, "a") as csv_file:
            writer = csv.writer(csv_file, delimiter=";")
            deep_res = self.parse_deep(d)

            tab = [element_number]

            for i in range(self.params["count_param"] + 3):
                tab.append(x[i])

            for i in range(self.params["count_param"]):
                tab.append(x_b[i])

            for i in range(self.params["count_param"]):
                tab.append(x_deep[i])

            for i in range(self.params["count_param"]):
                tab.append(x_deep_b[i])

            for xx in [iteration, element_number, curr_seed, o_alpha, alpha, accept]:
                tab.append(xx)

            for res in deep_res:
                tab.append(res)

            writer.writerow(tab)
