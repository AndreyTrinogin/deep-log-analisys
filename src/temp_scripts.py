from src.log_parser import parse_log_result, parse_log_deep


def collect_iters_strings(log_result_file_file_name):
    d = {}
    with open(log_result_file_file_name) as log_result_file:
        for line in log_result_file:
            if line.startswith('iteration'):
                iter_num = line.split("=")[1].strip()
                if iter_num not in d.keys():
                    d[iter_num] = 1
                else:
                    d[iter_num] += 1
    return d


def print_1_lvl_dict(d: dict):
    for elem in d:
        print('{}:{}'.format(elem, d[elem]))


def print_2_lvl_dict(d: dict):
    for it in d:
        print(it)
        for elem in d[it]:
            print('{}:{}'.format(elem, d[it][elem]))


if __name__ == "__main__":
    
    start_res, res, cnt = parse_log_result('../test_data_cutted/log_res_cut.txt', 40, 8)
    print_1_lvl_dict(start_res)
    print("kek")
    print_1_lvl_dict(res)
    print(cnt)

    deep_start_data, deep_data = parse_log_deep('../test_data_cutted/log_deep_ct', 67, 40, 8)
    # print_2_lvl_dict(deep_start_data)
    # print("kek")
    # print_2_lvl_dict(deep_data)

    # iter_st_data, iter_data = parse_log_iteration('log_iteration.txt', {
    #     'count_iter': 501, 'start_iter': 40,
    #     'count_param': 4, 'count_theta': 8
    # })
    #
    # print_2_lvl_dict(iter_st_data)
    # print('kek')
    # print_2_lvl_dict(iter_data)

    # d = {
    #     'count_iter': 501, 'start_iter': 40,
    #     'count_param': 4, 'count_theta': 8
    # }
    #
    # deep_reader = deep_reader.DeepReader(d)
