import logging as lg
from datetime import datetime


def _is_string_without_definition_iter(lst):
    return len(lst) > 3


def is_string_iter_def(lst):
    return len(lst) == 3


def get_default_logger(log_level: int):
    lg.basicConfig(handlers=[lg.FileHandler('analysis_log.txt'), lg.StreamHandler()], level=log_level)


def get_time():
    return str(datetime.now())
